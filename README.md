
# Quando usar IDs ou Classes?

Nós usamos os seletores do CSS para encontrar no arquivo HTML o elemento que queremos estilizar. É através do seletor que conseguimos chegar no ponto específico que necessita de um estilo, ou aplicar um estilo geral, sem ter que descrever a estilização em elemento por elemento.

Classes são necessárias quando há mais de um elemento parecido em tela ao qual você não deseja escrever o estilo de cada um separadamente. Também são úteis para manter o padrão página a página dentro de um site.Exemplos:

1. Cards em um site de e-commerce costumam ser todos do mesmo formato e estilo.
2. Títulos em notícias que seguem sempre o mesmo padrão de fonte, cor e tamanho.
3. Botões que se repetem durante páginas sem alterar estilo.
Se você perceber que o estilo de dois elementos são iguais, talvez valha a pena criar uma classe para defini-los. Exemplo:

```
/**
 * Aplica um estilo em comum para todos os botões da página
 */
.botao {
    padding: 10px;
    border-radius: 10px;
    font-weight: bold;
    background-color: rgb(228, 228, 228);
    border: none;
}

/**
 * Aplica um estilo em comum para todos os botões de success que poderão ser usando em formulários com textos de:
 * - Enviar
 * - Submeter
 * - Concordo
 */
.botao-success {
    background-color: rgb(130, 246, 100);
}

/**
 * Aplica um estilo em comum para todos os botões de danger que poderão ser usando em formulários com textos de:
 * - Remover
 * - Deletar
 * - Não Concordo
 */
.botao-danger {
    background-color: rgb(255, 91, 91);
}
```

Por outro lado, o ID nós usamos sempre que algum elemento é tão específico, que nenhum outro terá uma estilização parecida. Como por exemplo, um botão comemorativo que leva para uma tela de aniversário de empresa ou eventos especiais:

```
/**
* Aplica um estilo único para o botão, para que possa ser usado em ocasiões especiais
*/
#botao-comemorativo-grande {
    width: 150px;
    height: 150px;
    background-color: yellow;
    padding: 30px;
    font-size: 20px;
}
```

Tente trocar caminhos gigantescos por classes e IDs para melhorar a performance do código:

Ao invés de `body section ul li {}` use `.details-list {}`
Ao invés de `body section > div > p > span {}` use `.message-span {}`
